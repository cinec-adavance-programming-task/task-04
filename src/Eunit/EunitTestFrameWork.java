package Eunit;
import java.util.*;
import java.lang.reflect.Modifier;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class EunitTestFrameWork {
	
	  private static List<String> exceptionReports = new ArrayList<>();
	  private static List<String> checks;
	  private static int checksMade = 0;
	  private static int passedChecks = 0;
	  private static int failedChecks = 0;
	  
	  private static void addToReport(String txt) {
		    if (checks == null) {
		      checks = new LinkedList<String>();
		    }
		    checks.add(String.format("%04d: %s", checksMade++, txt));
		  }

		  public static void checkEquals(double value1, double value2) {
		    if (value1 == value2) {
		      addToReport(String.format("  %2f == %2f", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %2f == %2f", value1, value2));
		      failedChecks++;
		    }
		  }


		  public static void checkNotEquals(double value1, double value2) {
		    if (value1 != value2) {
		      addToReport(String.format("  %2f != %2f", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %2f != %2f", value1, value2));
		      failedChecks++;
		    }
		  }


		  public static void report() {
		    System.out.printf("%d checks passed\n", passedChecks);
		    System.out.printf("%d checks failed\n", failedChecks);
		    System.out.println();
		    
		    for (String check : checks) {
		      System.out.println(check);
		    }
		  }

	  public static void main(String[] args) {
		  
//	    TestSimple ts = new TestSimple();
//	    runCheckMethods(ts);
//	    Simple s = new Simple(5.0, 5.0);
//	    assertVariableValue(s, "firstaNumber", Double.class, 5.0);
//	    assertMethodResult(s, "squareA", new Class[]{Double.class}, new Object[]{5.0}, 5.0);
//	    report();
	  }
	  //Launcher
	  
	  public static void runCheckMethods(Object obj) {
	      Class<?> clazz = obj.getClass();
	      Method[] methods = clazz.getDeclaredMethods();
	      for (Method method : methods) {
	          if (isCheckMethod(method)) {
	              try {
	              
	                  if (!Modifier.isPublic(method.getModifiers())) {
	                      method.setAccessible(true);
	                  }

	                  method.invoke(obj);
	              } catch (Exception e) {
	            
	            	  exceptionReports.add("Exception in method " + method.getName() + ": " + e.getMessage());
	                  e.printStackTrace();
	              }
	          }
	      }
	  }
	  
	  
	  
	  public static boolean isCheckMethod(Method method) {
	      return method.getName().startsWith("check");
	  }
	  
	 
	  
	  public static void assertVariableValue(Object obj, String variableName, Class<?> variableType, Object expectedValue) {
	      try {
	          Field field = obj.getClass().getDeclaredField(variableName);
	          if (!Modifier.isPublic(field.getModifiers())) {
	              field.setAccessible(true);
	          }

	          Object actualValue = field.get(obj);
	      ;

	          if (!variableType.isInstance(actualValue) || !actualValue.equals(expectedValue)) {
	              throw new AssertionError("Assertion failed for variable " + variableName +
	                      ". Expected: " + expectedValue + " of type " + variableType.getSimpleName() +
	                      ", Actual: " + actualValue + " of type " + actualValue.getClass().getSimpleName());
	          }
	      } catch (Exception e) {
	    	
	          exceptionReports.add("Exception in assertVariableValue for variable " + variableName + ": " + e.getMessage());
	      }
	  }
	  
	
	  public static void assertMethodResult(Object obj, String methodName, Class<?>[] parameterTypes, Object[] args, Object expectedResult) {
	      try {
	          Method method = obj.getClass().getDeclaredMethod(methodName, parameterTypes);
	          if (!Modifier.isPublic(method.getModifiers())) {
	              method.setAccessible(true);
	          }

	          Object actualResult = method.invoke(obj, args);
	          System.out.println(actualResult);

	          if (!actualResult.equals(expectedResult)) {
	              throw new AssertionError("Assertion failed for method " + methodName +
	                      ". Expected: " + expectedResult +
	                      ", Actual: " + actualResult);
	          }
	      } catch (Exception e) {
	          exceptionReports.add("Exception in assertMethodResult for method " + methodName + ": " + e.getMessage());
	      }
	  }
}
