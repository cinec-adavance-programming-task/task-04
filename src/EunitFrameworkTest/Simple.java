package EunitFrameworkTest;

public class Simple {

  public double height = 10.0;
  private double width = 20.0;
  
  public Simple() {
  }
  
  public Simple(double a, double b) {
    this.height = a;
    this.width = b;
  }

  public void squareA(){
    this.height *= this.height;
  }

  private void squareB(){
    this.width *= this.width;
  }

  public double getA() {
    return height;
  }

  private void setA(double a) {
    this.height = a;
  }

  public double getB() {
    return width;
  }

  public void setB(double b) {
    this.width = b;
  }
  
  public String toString() {
    return String.format("(a:%d, b:%d)", height, width);
  }

}
