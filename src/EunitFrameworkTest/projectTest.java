package EunitFrameworkTest;
import static Eunit.EunitTestFrameWork.*;

import java.lang.reflect.Method;

import Eunit.EunitTestFrameWork;

public class projectTest {
	
	  void checkConstructorAndAccess(){
		    Simple s = new Simple(30.0, 41.32);
		    Method currentMethod = null;
	        try {
	            // Assuming that isCheckMethod is in the same class (projectTest)
	            currentMethod = getClass().getDeclaredMethod("checkConstructorAndAccess");
	        } catch (NoSuchMethodException | SecurityException e) {
	            e.printStackTrace(); // Handle the exception according to your needs
	        }

	        if (currentMethod != null) {
	            boolean result = EunitTestFrameWork.isCheckMethod(currentMethod);
	            System.out.println("Result: " + result);
	        }
		    
		  }

		  void checkSquareA(){
		    Simple s = new Simple(30.46, 40.633434);
		    s.squareA();
		    checkEquals(s.getA(), 9.90944);
		  }

		  public static void main(String[] args) {
			projectTest ts = new projectTest();
		    ts.checkConstructorAndAccess();
		    ts.checkSquareA();
		    report();
		  }
}
